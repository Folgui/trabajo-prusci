(function() {
  // Run Animation
  var frameLoop;
  // Loop Animation
  frameLoop = setInterval(function() {
    return $('.menu').toggleClass('active');
  }, 3000);
  // Disables Loop onHover
  $('body').hover(function() {
    return clearInterval(frameLoop);
  });

  $('body').click(function() {
    return clearInterval(frameLoop);
  });

  $('.fab').on('click', function(e) {
    e.preventDefault();
    if($('.menu').hasClass('active')){
      $('.button').hide();
    }
    else{
      $('.button').show();
    }
    return $('.menu').toggleClass('active');
  });

  window.onload = function() {
    return clearInterval(frameLoop);
  };

  $('#reset').on('click', function(){
    $("#accept").off("click");
    $('#accept').on('click', function(){
      console.log('Resetea!!!!!!');
       $('.pop-up').removeClass('open');
        chatSocket.send(JSON.stringify({
          message : "resetea"
        }));
    });
  });

  $('#emergency-alarm').on('click', function(){
    $("#accept").off("click");
    $('#accept').on('click', function(){
      console.log('Emergencia!!!!!!');
      $('.pop-up').removeClass('open');
        chatSocket.send(JSON.stringify({
          message : "emergencia"
        }));
    });
  });

}).call(this);


/*window.onload = function() {
  return $('.menu').toggleClass('active');
};*/