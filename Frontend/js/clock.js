var module = angular.module('clockApp', []);

module.controller('TimeController', function($scope, $filter, $interval) {
  const slider1 = document.getElementById("slider1");
  const slider2  = document.getElementById("slider2");

  var tick = function() {
    $scope.date = Date.now();
    $scope.getTime();
  }

  $scope.changeFormat = function() {
    $scope.hours24 = !$scope.hours24;
    $scope.getTime();
  }
  $scope.getTime = function() {
    $scope.time = $filter('date')($scope.date, 'HH:mm');
    slider1.style.opacity = 0.5;
    slider2.style.opacity = 1;
  }
  tick();
  $interval(tick, 1000);
});

/*$scope.hours24 = true;*/
/*if($scope.hours24 == true) {
$scope.time = $filter('date')($scope.date, 'h:mm');
slider1.style.opacity = 1;
slider2.style.opacity = 0.5;
} else {
$scope.time = $filter('date')($scope.date, 'HH:mm');
slider1.style.opacity = 0.5;
slider2.style.opacity = 1;
}*/