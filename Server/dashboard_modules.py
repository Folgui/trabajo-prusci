from django.utils.translation import ugettext_lazy as _
from jet.dashboard import modules
from kronosapp.modules import *
from jet.dashboard.dashboard import Dashboard, AppIndexDashboard

class CustomDashboard(Dashboard):
    columns = 4

    def init_with_context(self, context):
        self.available_children.append(RingtoneDashboard)
        self.children.append(RingtoneDashboard(
            _('Ringtones'),
            column=0,
            order=0
        ))
        self.available_children.append(RingtimeDashboard)
        self.children.append(RingtimeDashboard(
            _('Ringtimes'),
            column=1,
            order=0
        ))
        self.available_children.append(DoNotRingDashboard)
        self.children.append(DoNotRingDashboard(
            _('Holidays'),
            column=2,
            order=0
        ))

