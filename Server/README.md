kronos
Added services runserver and ring:
1)Install supervisor:
    aptitude install supervisor
2)create ring.conf 

[program:ring]
command=/home/jgros/.virtualenvs/kronos/bin/python3 /home/jgros/Repositorios/kronos/manage.py ring
process_name=ring
numprocs=1
directory=/tmp
umask=022
priority=999
autostart=true
autorestart=true
startsecs=10
startretries=3
exitcodes=0,2
stopsignal=TERM
stopwaitsecs=10
user=jgros
redirect_stderr=false
stdout_logfile=/var/log/supervisor/ring.log
stdout_logfile_maxbytes=10MB
stdout_logfile_backups=10
stdout_capture_maxbytes=1MB
stderr_logfile=/var/log/supervisor/ring-error.log
stderr_logfile_maxbytes=1MB
stderr_logfile_backups=10
stderr_capture_maxbytes=1MB
serverurl=AUTO


and runserver.conf 

[program:runserver]
command=/home/jgros/.virtualenvs/kronos/bin/python3 /home/jgros/Repositorios/kronos/manage.py runserver 0.0.0.0:8000 --noreload
process_name=runserver
numprocs=1
directory=/tmp
umask=022
priority=999
autostart=true
autorestart=true
startsecs=10
startretries=3
exitcodes=0,2
stopsignal=TERM
stopwaitsecs=5
user=jgros
redirect_stderr=false
stdout_logfile=/var/log/supervisor/runserver.log
stdout_logfile_maxbytes=10MB
stdout_logfile_backups=10
stdout_capture_maxbytes=1MB
stderr_logfile=/var/log/supervisor/runserver-error.log
stderr_logfile_maxbytes=1MB
stderr_logfile_backups=10
stderr_capture_maxbytes=1MB
serverurl=AUTO


in etc/supervisor/

3)sudo supervisorctl reread
4)sudo service supervisor restart
