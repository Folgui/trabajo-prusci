from jet.dashboard.modules import *
from django.utils.translation import ugettext_lazy as _
from .models import *
from .templates import *


class RingtimeDashboard(DashboardModule):
    title = _('Ringtimes')
    limit = 10

    template = 'ringtime.html'
    layout = 'stacked'
    children = []
    def init_with_context(self, context):
        ringtimes = Ringtime.objects.all()
        ringtimes_by_id = []
        for item in ringtimes:
            for item2 in item.days.all():
                if item2.id == datetime.datetime.today().weekday()+1:
                    if item.hour >= datetime.datetime.today().now().time():
                        ringtimes_by_id.append(item.id)

        self.children = Ringtime.objects.filter(id__in=ringtimes_by_id).order_by('hour')[:self.limit]


class RingtoneDashboard(DashboardModule):
    title = _('Ringtones')
    limit = 10

    template = 'ringtone.html'
    layout = 'stacked'
    children = []
    def init_with_context(self, context):
        self.children = Ringtone.objects.all().order_by('type')[:self.limit]


class DoNotRingDashboard(DashboardModule):
    title = _('Do Not Ring')
    limit = 10

    template = 'do_not_ring.html'
    layout = 'stacked'
    children = []

    def init_with_context(self, context):
        do_not_rings = DoNotRing.objects.all()
        do_not_rings_by_id = []
        for item in do_not_rings:
            if item.date.month == datetime.datetime.today().month and item.date.day >= datetime.datetime.today().day:
                do_not_rings_by_id.append(item.id)
        self.children = DoNotRing.objects.filter(id__in=do_not_rings_by_id).order_by('date')[:self.limit]


class RecentActions(RecentActions):
    pass
