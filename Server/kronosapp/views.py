from django.http import HttpResponse
from django.shortcuts import render
from django.utils.safestring import mark_safe
import json


def index(request):
    return HttpResponse("Hello, world. You're at the kronosapp index.")

# Create your views here.
