import urllib.request, json
from kronosapp.models import DoNotRing
from django.core.management.base import BaseCommand, CommandError
from datetime import datetime

class Command(BaseCommand):
    help = 'Transfers the google calendar to the holidays model'

    def handle(self, *args, **options):

        url = "https://www.googleapis.com/calendar/v3/calendars/es.ar%23holiday%40group.v.calendar.google.com/events?key=AIzaSyD5Sulo-fpWDygs35hnx9qprbuKh7ctFoA"
        response = urllib.request.urlopen(url)
        calendar = json.loads(response.read().decode("utf-8"))

        holidays = DoNotRing.objects.all()
        for do_not_ring in holidays:
            do_not_ring.updated = False

        for holiday in calendar['items']:
            holiday['start']['date'] = datetime.strptime(holiday['start']['date'], "%Y-%m-%d").date()
            if holiday['start']['date'].year == datetime.now().year:
                is_new = True
                for do_not_ring in holidays:
                    if do_not_ring.type == holiday['summary']:
                        if not do_not_ring.updated:
                            do_not_ring.date = holiday['start']['date']
                            do_not_ring.updated = True
                            do_not_ring.save()
                            is_new = False
                            break
                        is_new = False
                if is_new:
                    DoNotRing(type=holiday['summary'], date=holiday['start']['date'], updated=True).save()
