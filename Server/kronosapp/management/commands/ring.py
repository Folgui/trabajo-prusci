from kronosapp.models import *
from django.core.management.base import BaseCommand
from datetime import datetime
import time
#import RPi.GPIO as GPIO

class Command(BaseCommand):
    help = 'Prints in terminal when it has to ring'

    def handle(self, *args, **options):
        # GPIO.setmode(GPIO.BCM)
        # GPIO.setup(17, GPIO.OUT)
        last = datetime.now().minute
        while True:
            if last == datetime.now().minute:
                time.sleep(1)
                continue
            last = datetime.now().minute
            ringtimes = Ringtime.objects.all()
            ringtimes_by_id = []
            for item in ringtimes:
                for item2 in item.days.all():
                    if item2.id == datetime.today().weekday() + 1:
                        ringtimes_by_id.append(item)
            for rings in ringtimes_by_id:
                if rings.hour.hour == datetime.now().hour and rings.hour.minute == datetime.now().minute:
                    ringtone = Ringtone.objects.filter(id=rings.ringtone.id)[0]
                    for item in ringtone.song:
                        if item == "1":
                            #GPIO.output(17, GPIO.HIGH)
                            pass
                        else:
                            #GPIO.output(17, GPIO.LOW)
                            pass
                        time.sleep(1)
                    print("RIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIING", rings.hour)
            time.sleep(1)
            #GPIO.output(17, GPIO.LOW)
            #GPIO.cleanup()