# Generated by Django 2.0.4 on 2018-05-10 12:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kronosapp', '0004_auto_20180510_0848'),
    ]

    operations = [
        migrations.AlterField(
            model_name='donotring',
            name='description',
            field=models.CharField(max_length=100, verbose_name='Description'),
        ),
        migrations.AlterField(
            model_name='ringtime',
            name='hour',
            field=models.TimeField(verbose_name='Hour'),
        ),
    ]
