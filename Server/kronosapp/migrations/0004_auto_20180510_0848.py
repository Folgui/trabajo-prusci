# Generated by Django 2.0.4 on 2018-05-10 11:48

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kronosapp', '0003_relationalentity'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='relationalentity',
            name='ringtime',
        ),
        migrations.RemoveField(
            model_name='relationalentity',
            name='weekday',
        ),
        migrations.DeleteModel(
            name='RelationalEntity',
        ),
    ]
