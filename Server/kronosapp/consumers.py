# chat/consumers.py
from channels.generic.websocket import WebsocketConsumer
import json
import time
from .models import *
from datetime import datetime



class ChatConsumer(WebsocketConsumer):
    connected=False
    time = None

    def connect(self):
        self.accept()
        self.send(text_data=json.dumps({
            'message': "Conectado"}))
        self.connected=True
        while self.connected:
            self.sendStatus()
            time.sleep(1)

    def disconnect(self, close_code):
        self.connected = False

    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']
        

    def sendStatus(self):
        item3 = ''
        ring = Ringtime.objects.all().order_by('-hour')
        for item in ring:
            item3 = item
            break
        ring = Ringtime.objects.all().order_by('hour')
        for item in ring:
            for item2 in item.days.all():
                if item2.id == datetime.today().weekday()+1:
                    if item.hour >= datetime.now().time().replace(microsecond=0):
                        x = item.hour
                        if self.time != x:
                            self.time = x
                            self.send(text_data=json.dumps({
                                'hour': x.strftime('%H:%M'),
                                'color': item3.color,
                                'desc': item3.description,
                            }))
                        return
            item3 = item


