from django.contrib import admin
from .models import *

# Register your models here.


class DaysInline(admin.StackedInline):
    extra = 0


@admin.register(DoNotRing)
class DoNotRingAdmin(admin.ModelAdmin):
    list_display = ('type', 'date')


@admin.register(Ringtime)
class RingtimeAdmin(admin.ModelAdmin):
    list_display = ('hour', 'get_ringtone')

    def get_ringtone(self, obj):
        return obj.ringtone.type

    get_ringtone.admin_order_field = 'ringtone'
    get_ringtone.short_description = 'Ringtone Name'


@admin.register(Ringtone)
class RingtoneAdmin(admin.ModelAdmin):
    list_display = ('type', 'song')
