from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User


class Ringtone(models.Model):
    type = models.CharField(max_length=45)
    pre_defined = models.BooleanField (default=False, editable=False)
    song = models.CharField(max_length=200)

class DoNotRing(models.Model):
    type = models.CharField(max_length=80)
    date = models.DateField(auto_now=False)
    updated = models.BooleanField(default=False, editable=False)


class WeekDays(models.Model):
    day = models.IntegerField()

    def __str__(self):
        week_list = [_("Monday"), _("Tuesday"), _("Wednesday"), _("Thursday"), _("Friday"), _("Saturday"), _("Sunday")]
        return str(week_list[self.day])

def get_pre_defined():
    return Ringtone.objects.filter(pre_defined=True)[0]

class Ringtime(models.Model):
    ringtone = models.ForeignKey(Ringtone, null=True, on_delete=models.SET(get_pre_defined))
    hour = models.TimeField(auto_now=False, verbose_name=_("Hour"))
    days = models.ManyToManyField('WeekDays')
    color = models.CharField(max_length=10,blank=True)
    description = models.CharField(max_length=15,blank=True)




