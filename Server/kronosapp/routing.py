from django.conf.urls import url
from django.urls import path, include

from . import consumers

websocket_urlpatterns = [
    url(r'^ws/kronosapp/room', consumers.ChatConsumer),
]
