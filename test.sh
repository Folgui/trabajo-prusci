#!/bin/bash

: '
 ========================================
 Bash script to install kronos app.

 to execute this instalation file use:
 $ sudo chmod +x test.sh
 $ sudo ./test.sh
 ========================================
'

: ' OPEN
 reboot autostart chromium-browser
' 


if [ -d ~/.config/autostart ]; then
    rm -rf ~/.config/autostart
else 
    mkdir ~/.config/autostart
fi

touch ~/.config/autostart/chromium-browser-autostart.desktop
echo ' 
[Desktop Entry]	 
Type=Application
Name=Chromium autostart
Exec=chromium-browser --koisk "(/~/Desktop/Frontend/index.html)" ' > ~/.config/autostart/chromium-browser-autostart.desktop


: '

	reboot


 CLOSE
 reboot autostart chromium-browser
'